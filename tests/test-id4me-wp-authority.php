<?php
require_once './includes/class-id4me-wp-authority.php';

class Test_ID4me_WP_Authority extends WP_UnitTestCase {

	/**
	 * @test ID4me_WP_Authority::has_expired()
	 * @dataProvider provider_has_expired
	 *
	 * @param string $expired
	 * @param boolean $has_expired
	 *
	 * @throws Exception
	 */
	public function test_has_expired( $expired, $has_expired ) {

		$authority = $this->_get_dummy_wp_authority();
		$authority->set_expired( $expired );

		$this->assertSame(
			$authority->has_expired(),
			$has_expired
		);
	}

	/**
	 * Data Provider for test_has_expired()
	 *
	 * @return array
	 */
	public function provider_has_expired() {

		return array(
			array(
				'2050-01-01',
				false
			),
			array(
				'2001-01-01',
				true
			),
			array(
				'',
				false
			),
			array(
				null,
				false
			)
		);
	}

	/**
	 * Initialize an ID4me_WP_Authority authority Mock Object
	 *
	 * @return ID4me_WP_Authority
	 */
	private function _get_dummy_wp_authority() {

		$authority = new ID4me_WP_Authority();
		$authority->set_id( 1 );
		$authority->set_hostname( 'dummy-authority.com' );
		$authority->set_client_id( 'abcd123' );
		$authority->set_client_secret( 'qefeq6g47r8ghhet9rht9rh8t6' );
		$authority->set_configuration( '{[]}' );

		return $authority;
	}
}
