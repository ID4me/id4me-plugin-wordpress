<?php
require_once './includes/class-id4me-user.php';

class Test_ID4me_User extends WP_UnitTestCase {

	/**
	 * @test Test_ID4me_WP_User_Meta::get_wp_user()
	 * @dataProvider provider_get_wp_user
	 *
	 * @param string $identifier
	 * @param string $iss
	 * @param string $sub
	 * @param boolean $user_has_been_found
	 */
	public function test_get_wp_user( $identifier, $iss, $sub, $user_has_been_found ) {

		$authorized_identifier = 'identifier.com';
		$authorized_sub = 'sub.com';
		$authorized_iss = 'iss.com';

		$user_id = $this->factory->user->create();
		update_user_meta( $user_id, 'id4me_identifier', $identifier );

		if ( ! is_null( $iss ) ) {
			update_user_meta( $user_id, 'id4me_iss', $iss );
		}
		if ( ! is_null( $sub ) ) {
			update_user_meta( $user_id, 'id4me_sub', $sub );
		}
		try {
			$user_meta = new ID4me_User( $authorized_identifier, $authorized_sub, $authorized_iss );
			$user = $user_meta->get_wp_user();
			$found = ( $user instanceof WP_User );

		} catch ( Exception $exception ) {
			$found = false;
		}

		if ( $found ) {
			$iss_from_db = get_user_meta( $user_id, 'id4me_iss', true );
			$sub_from_db = get_user_meta( $user_id, 'id4me_sub', true );

			$this->assertSame( $authorized_sub, $sub_from_db );
			$this->assertSame( $authorized_iss, $iss_from_db );
		}

		$this->assertSame( $user_has_been_found, $found );
	}

	/**
	 * Data Provider for test_get_wp_user()
	 *
	 * @return array
	 */
	public function provider_get_wp_user() {

		return array(
			array(
				'identifier.com',
				'iss.com',
				'sub.com',
				true,
			),
			array(
				'',
				'iss.com',
				'sub.com',
				true,
			),
			array(
				'identifier.com',
				'',
				'sub.com',
				false,
			),
			array(
				'identifier.com',
				'iss.com',
				'',
				false,
			),
			array(
				'',
				'',
				'',
				false,
			),
			array(
				'identifier.com',
				'',
				'',
				true,
			),
			array(
				'identifier.com',
				null,
				null,
				true,
			),
		);
	}

	/**
	 * @test Test_ID4me_WP_User_Meta::get_wp_user()
	 * (Special case test when same identifiers are assigned to more than one user)
	 * @dataProvider provider_get_wp_user_duplicate_entries
	 *
	 * @param string $identifier1
	 * @param string $iss1
	 * @param string $sub1
	 * @param string $identifier2
	 * @param string $iss2
	 * @param string $sub2
	 * @param boolean $user_has_been_found
	 */
	public function test_get_wp_user_duplicated_entries( $identifier1, $iss1, $sub1, $identifier2, $iss2, $sub2, $user_has_been_found ) {
		$authorized_identifier = 'identifier.com';
		$authorized_sub = 'sub.com';
		$authorized_iss = 'iss.com';

		$user_id1 = $this->factory->user->create();
		update_user_meta( $user_id1, 'id4me_identifier', $identifier1 );
		if ( ! is_null( $iss1 ) ) {
			update_user_meta( $user_id1, 'id4me_iss', $iss1 );
		}
		if ( ! is_null( $sub1 ) ) {
			update_user_meta( $user_id1, 'id4me_sub', $sub1 );
		}

		$user_id2 = $this->factory->user->create();
		update_user_meta( $user_id2, 'id4me_identifier', $identifier2 );
		if ( null != $iss2 ) {
			update_user_meta( $user_id2, 'id4me_iss', $iss2 );
		}
		if ( null != $sub2 ) {
			update_user_meta( $user_id2, 'id4me_sub', $sub2 );
		}

		try {
			$user_meta = new ID4me_User( $authorized_identifier, $authorized_sub, $authorized_iss );
			$user = $user_meta->get_wp_user();
			$found = ( $user instanceof WP_User );

		} catch ( Exception $exception ) {
			$found = false;
		}

		$this->assertSame( $user_has_been_found, $found );
	}

	/**
	 * Data Provider for test_get_wp_user_duplicated_entries()
	 *
	 * @return array
	 */
	public function provider_get_wp_user_duplicate_entries() {

		return array(
			array(
				'identifier.com',
				'iss.com',
				'sub.com',
				'identifier.com',
				'iss.com',
				'sub.com',
				false,
			),
			array(
				'identifier.com',
				'iss.com',
				'sub.com',
				'identifier2.com',
				'iss.com',
				'sub.com',
				false,
			),
			array(
				'identifier.com',
				'iss.com',
				'sub.com',
				'identifier.com',
				'iss2.com',
				'sub2.com',
				true,
			),
			array(
				'identifier_1.com',
				'iss.com',
				'sub.com',
				'identifier_2.com',
				'iss2.com',
				'sub2.com',
				true,
			),
			array(
				'identifier_1.com',
				'iss.com',
				'sub.com',
				'identifier.com',
				'iss2.com',
				'sub2.com',
				true,
			),
			array(
				'identifier.com',
				'iss.com',
				'sub2.com',
				'identifier_2.com',
				'iss.com',
				'sub3.com',
				false,
			),
			array(
				'identifier.com',
				'',
				'',
				'identifier_2.com',
				'',
				'',
				true,
			),
			array(
				'identifier.com',
				null,
				null,
				'identifier_2.com',
				'',
				'',
				true,
			),
			array(
				'identifier.com',
				'',
				'',
				'identifier.com',
				'',
				'',
				false,
			),
		);
	}

	/**
	 * @test Test_ID4me_WP_User_Meta::get_user_by_sub_and_iss()
	 */
	public function test_get_user_by_sub_and_iss() {
		$user_id = $this->factory->user->create();
		$sub = 'test';
		$iss = 'domain.com';
		$identifier = 'domain.com';
		update_user_meta( $user_id, 'id4me_sub', $sub );
		update_user_meta( $user_id, 'id4me_iss', $iss );
		$user_meta = new ID4me_User( $identifier, $sub, $iss );
		$user = $user_meta->get_user_by_sub_and_iss();

		$this->assertSame( count( $user ), 1 );
	}

	/**
	 * @test Test_ID4me_WP_User_Meta::get_user_by_identifier()
	 * @dataProvider provider_get_user_by_identifier
	 *
	 * @param $identifier
	 * @param $sub
	 * @param $iss
	 * @param $expected_user_count
	 */
	public function test_get_user_by_identifier( $identifier, $sub, $iss, $expected_user_count ) {
		$user_id = $this->factory->user->create();
		$user_meta = new ID4me_User( $identifier, $sub, $iss );
		update_user_meta( $user_id, 'id4me_identifier', $identifier );
		update_user_meta( $user_id, 'id4me_sub', $sub );
		update_user_meta( $user_id, 'id4me_iss', $iss );
		$user = $user_meta->get_user_by_identifier();

		$this->assertSame( $expected_user_count, count( $user ) );
	}

	/**
	 * Data Provider for test_get_user_by_identifier()
	 * @return array
	 */

	public function provider_get_user_by_identifier() {
		return array(
			array(
				'identifier',
				'',
				'',
				1
			),
			array(
				null,
				'',
				'',
				0
			),
		);
	}

	/**
	 * @test Test_ID4me_WP_User_Meta::save_user_meta_registration_data()
	 * @dataProvider provider_save_user_meta_registration_data
	 *
	 * @param $user_id
	 * @param $nick_name
	 * @param $website
	 * @param $family_name
	 * @param $given_name
	 */
	public function test_save_user_meta_registration_data( $user_id, $nick_name, $website, $family_name, $given_name ) {
		$user_data = array(
			'id4me_nickname' => $nick_name,
			'id4me_website'  => $website,
			'last_name'      => $family_name,
			'first_name'     => $given_name
		);
		ID4me_User::save_user_meta_registration_data( $user_id, $nick_name, $website, $family_name, $given_name );
		$user = get_user_meta( $user_id );
		if ( ! empty( $user_id ) ) {
			foreach ( $user_data as $key => $data ) {
				$this->assertSame( $data, $user[ $key ][0] );
			}
		} else {
			$this->assertSame( false, $user );
		}
	}

	/**
	 * Data Provider for test_save_user_meta_registration_data()
	 * @return array
	 */
	public function provider_save_user_meta_registration_data() {
		return array(
			array(
				1,
				"nick_name",
				"website",
				"family_name",
				"given_name"
			),
			array(
				'',
				"nick_name",
				"website",
				"family_name",
				"given_name"
			),
		);
	}
}
