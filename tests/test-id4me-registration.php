<?php

use Id4me\RP\Model\ClaimRequest;
use Id4me\RP\Model\ClaimRequestList;

class Test_Id4me_Registration extends WP_UnitTestCase {

	/**
	 * @test Registration::get_claimlist_for_registration()
	 * @dataProvider provider_get_claimlist_for_registration
	 *
	 * @param $claimlist_expected
	 */
	public function test_get_claimlist_for_registration( $claimlist_expected ) {
		$claimlist = ID4me_Registration::get_claimlist_for_registration();
		$this->assertEquals( $claimlist_expected, $claimlist );
	}

	/**
	 * Data Provider for test_get_claimlist_for_registration()
	 * @return array
	 */
	public function provider_get_claimlist_for_registration() {
		return array(
			array(
				new ClaimRequestList(
					new ClaimRequest( 'email', true ),
					new ClaimRequest( 'nickname' ),
					new ClaimRequest( 'given_name' ),
					new ClaimRequest( 'family_name' ),
					new ClaimRequest( 'preferred_username' ),
					new ClaimRequest( 'website' ),
					new ClaimRequest( 'id4me.identifier' )
				)
			)
		);
	}

	/**
	 * @test Registration::validate_form()
	 *
	 * @param $same_user_exists
	 * @param $errors_passed
	 * @param $throws_exception
	 *
	 * @dataProvider provider_validate_form
	 */
	public function test_validate_form( $same_user_exists, $errors_passed, $throws_exception ) {
		$registration = new ID4me_Registration();

		if ( $errors_passed ) {
			if ( ! $throws_exception ) {
				$_POST['id4me_errors'] = wp_json_encode( array( 'error' ) );
			} else {
				$_POST['id4me_errors'] = wp_json_encode( array( new Exception() ) );
			}
		} else {
			$_POST['id4meIdentifier'] = 'dummy_identifier';
			$_POST['id4me_sub'] = 'dummy_sub';
			$_POST['id4me_iss'] = 'dummy_iss';
		}
		if ( $same_user_exists ) {
			ID4me_User::save_iss_sub_identifier( 1, $_POST['id4me_sub'], $_POST['id4me_iss'], $_POST['id4meIdentifier'] );
			$error = $registration->validate_form( new WP_Error() );
			$this->assertSame( $same_user_exists, ( count( $error->errors ) > 0 ) );
		} else {
			$error = $registration->validate_form( new WP_Error() );
			$this->assertSame( $same_user_exists, ( ( count( $error->errors ) > 0 ) && ( count( $error->error_data ) > 0 ) ) );
		}
	}

	/**
	 * Data Provider for test_validate_form()
	 *
	 * @return array
	 */
	public function provider_validate_form() {
		return array(
			array(
				true,
				true,
				true
			),
			array(
				true,
				true,
				false
			),
			array(
				true,
				false,
				true
			),
			array(
				true,
				false,
				false
			),
			array(
				false,
				false,
				true
			),
			array(
				false,
				false,
				true
			),
			array(
				false,
				true,
				true
			),
			array(
				false,
				true,
				true
			)
		);
	}
}