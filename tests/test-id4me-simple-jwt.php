<?php

require_once './includes/class-id4me-simple-jwt.php';
require_once './includes/class-id4me-authorization-state-model.php';

// Require exception classes
require_once './includes/exceptions/class-id4me-simple-jwt-exception.php';
require_once './includes/exceptions/class-id4me-simple-jwt-invalid-exception.php';
require_once './includes/exceptions/class-id4me-simple-jwt-invalid-header-exception.php';
require_once './includes/exceptions/class-id4me-simple-jwt-invalid-signature-exception.php';
require_once './includes/exceptions/class-id4me-simple-jwt-encoding-exception.php';

class Test_ID4me_Simple_Jwt extends WP_UnitTestCase {

	/**
	 * @test Test_ID4me_Simple_Jwt::getJWT()
	 * @dataProvider provider_get_jwt
	 *
	 * @param JsonSerializable|array $object
	 * @param string $key
	 * @param Type | null $exception
	 * @param string $expectedvalue
	 *
	 * @throws ID4me_Simple_Jwt_Encoding_Exception
	 */
	public function test_get_jwt( $object, $key, $throws, $expectedvalue ) {
		if ( $throws != null ) {
			$this->expectException( $throws );
		}

		$ret = ID4me_Simple_Jwt::get_jwt( $object, $key );

		$this->assertEquals( $expectedvalue, $ret, "Wrong JWT content" );
	}

	/**
	 * Data Provider for test_get_jwt()
	 *
	 * @return array
	 */
	public function provider_get_jwt() {

		return array(
			array(
				new AuthorizationStateModel( 'id', 'use-case' ),
				'secret',
				null,
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmlnaW4iOiJ1c2UtY2FzZSIsImlkZW50aWZpZXIiOiJpZCJ9.LPnmR-du5V3G4O3Ot4HOlA6DAB7zj-MeDqTd0kWfnBI'
			),
			array(
				array(
					'test'  => 'value',
					'test2' => 2,
					'test3' => array(
						'sub' => 'subval'
					)
				),
				'JBHASIUHFDIUABSgiosdfbbIBHIBFJUBFIhoshibascvbsdifbhiHBIABDSI',
				null,
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.9dgLn4MjFOBoJb_MtsnL6rWNeNIjfp2Y9Kj6dQgfnzI'
			),
			array(
				array(
					'test' => NAN
				),
				'secret',
				ID4me_Simple_Jwt_Encoding_Exception::class,
				null
			),
		);
	}

	/**
	 * @test Test_ID4me_Simple_Jwt::get_payload()
	 * @dataProvider provider_get_payload
	 *
	 * @param string $jwt
	 * @param string $key
	 * @param array|null $expectedvalue
	 *
	 * @throws ID4me_Simple_Jwt_Invalid_Exception
	 * @throws ID4me_Simple_Jwt_Invalid_Header_Exception
	 * @throws ID4me_Simple_Jwt_Invalid_Signature_Exception
	 */
	public function test_get_payload( $jwt, $key, $throws, $expectedvalue ) {
		if ( $throws != null ) {
			$this->expectException( $throws );
		}
		$ret = ID4me_Simple_Jwt::get_payload( $jwt, $key );
		$this->assertEquals( $expectedvalue, $ret, "Wrong JWT payload" );
	}

	/**
	 * Data Provider for test_get_payload()
	 *
	 * @return array
	 */
	public function provider_get_payload() {

		return array(
			array(
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmlnaW4iOiJ1c2UtY2FzZSIsImlkZW50aWZpZXIiOiJpZCJ9.LPnmR-du5V3G4O3Ot4HOlA6DAB7zj-MeDqTd0kWfnBI',
				'secret',
				null,
				array(
					'identifier' => 'id',
					'origin'     => 'use-case'
				)
			),
			array(
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.9dgLn4MjFOBoJb_MtsnL6rWNeNIjfp2Y9Kj6dQgfnzI',
				'JBHASIUHFDIUABSgiosdfbbIBHIBFJUBFIhoshibascvbsdifbhiHBIABDSI',
				null,
				array(
					'test'  => 'value',
					'test2' => 2,
					'test3' => array(
						'sub' => 'subval'
					)
				),
			),
			array(
				'eyJhbGciOiJIUzI1NiJ9.H5g-iQloVogRIoEUto2gPipafg_-RE3e9h4NjIFHIO4',
				'nomatter',
				ID4me_Simple_Jwt_Invalid_Exception::class,
				null,
			),
			array(
				'eyJhbGciOiJIUzI1NiJ9.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.H5g-iQloVogRIoEUto2gPipafg_-RE3e9h4NjIFHIO4',
				'JBHASIUHFDIUABSgiosdfbbIBHIBFJUBFIhoshibascvbsdifbhiHBIABDSI',
				ID4me_Simple_Jwt_Invalid_Header_Exception::class,
				null,
			),
			array(
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.9dgLn4MjFOBoJb_MtsnL6rWNeNIjfp2Y9Kj6dQgfnzI',
				'wrongsecret',
				ID4me_Simple_Jwt_Invalid_Signature_Exception::class,
				null,
			),
			array(
				'InvalidHeader.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.9dgLn4MjFOBoJb_MtsnL6rWNeNIjfp2Y9Kj6dQgfnzI',
				'nomatter',
				ID4me_Simple_Jwt_Invalid_Header_Exception::class,
				null,
			),
			array(
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.InvalidPayload.LPnmR-du5V3G4O3Ot4HOlA6DAB7zj-MeDqTd0kWfnBI',
				'nomatter',
				ID4me_Simple_Jwt_Invalid_Exception::class,
				null,
			),
			array(
				'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZXN0IjoidmFsdWUiLCJ0ZXN0MiI6MiwidGVzdDMiOnsic3ViIjoic3VidmFsIn19.InvalidSignature',
				'nomatter',
				ID4me_Simple_Jwt_Invalid_Signature_Exception::class,
				null,
			),
		);
	}
}