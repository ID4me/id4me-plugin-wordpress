<?php

use Id4me\RP\Exception\InvalidAuthorityIssuerException;

require_once './includes/class-id4me-wp-authority.php';
require_once './includes/class-id4me-client.php';

class Test_ID4me_Client extends WP_UnitTestCase {

	public function tearDown() {
		global $id4me_errors;
		$id4me_errors = array();
	}

	/**
	 * @test ID4me_Client::discover_authority()
	 * @dataProvider provider_discover_authority
	 *
	 * @param string $authority_hostname
	 * @param boolean $throws_exception
	 * @param string $returned_authority_hostname
	 */
	public function test_discover_authority( $authority_hostname, $throws_exception, $returned_authority_hostname ) {
		$service = $this->_get_service_mock();

		if ( $throws_exception ) {
			$service->method( 'discover' )
				->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->method( 'discover' )
				->willReturn( $authority_hostname );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority_hostname = $id4me_client->discover_authority( 'wikipedia.org' );

		if ( empty( $authority_hostname ) || $throws_exception ) {
			$this->assertFalse( $returned_authority_hostname );

		} else {
			$this->assertEquals(
				$authority_hostname,
				$returned_authority_hostname
			);
		}
	}

	/**
	 * @test ID4me_Client::auth()
	 * @dataProvider provider_auth
	 *
	 * @param WP_User $user
	 * @param $origin
	 * @param $authority
	 * @param $authorize
	 * @param $expected_class
	 * @param $action
	 *
	 * @throws ID4me_Simple_Jwt_Encoding_Exception
	 */
	public function test_auth( $user, $origin, $authority, $authorize, $expected_class, $action ) {
		ID4me_WP_Authority::create_table();

		$user_info = $this->_get_user_info();
		$service = $this->_get_service_mock();
		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'discover' )->willReturn( $authority );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );
		$service->method( 'getUserInfo' )->willReturn( $user_info );
		$service->method( 'getAuthorizationUrl' )->willReturn( 'url' );
		$service->method( 'authorize' )->willReturn( $authorize );

		$id4me_client = new ID4me_Client( $service );

		$model = new AuthorizationStateModel( 'id4me_identifier', $origin );

		// Arrange
		$state = $id4me_client->get_state_from_model( $model );
		ID4me_User::save_iss_sub_identifier( 1, 'dummy', 'dummy', 'dummy' );

		$_GET['code'] = 'code';
		$_GET['state'] = $state;
		$_GET['id4me_action'] = $action;

		try {
			$returned_user = $id4me_client->auth( $user );
		} catch ( Exception $e ) {
			if ( strpos( $e, 'script' ) ) {
				$this->assertEquals( true, strpos( $e, 'window.close();' ) );
			}
		}
		if ( ( 'connect' === $origin || 'something' === $origin) && is_null( $user ) ) {
			if ( ! $authority ) {
				$this->assertInstanceOf( $expected_class, $returned_user );
			} else {
				$this->assertInstanceOf( $expected_class, $returned_user );
			}
		} else {
			$this->assertEquals( $user, $returned_user );
		}
		if ( 'auth' !== $action ) {
			$this->assertEquals( $user, $returned_user );
		}
	}

	/**
	 * Data Provider for test_auth()
	 *
	 * @return array
	 */
	public function provider_auth() {

		return array(
			array(
				new WP_User( 1 ),
				'register',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'register',
				'dummy-authority.com',
				false,
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'register',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'register',
				false,
				false,
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'authorize',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'authorize',
				'dummy-authority.com',
				false,
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'authorize',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'authorize',
				false,
				false,
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'connect',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'connect',
				'dummy-authority.com',
				false,
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'connect',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				new WP_User( 1 ),
				'connect',
				false,
				false,
				null,
				'auth',
			),
			array(
				null,
				'register',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				null,
				'register',
				'dummy-authority.com',
				false,
				null,
				'auth',
			),
			array(
				null,
				'register',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				null,
				'register',
				false,
				false,
				null,
				'auth',
			),
			array(
				null,
				'authorize',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				null,
				'authorize',
				'dummy-authority.com',
				false,
				null,
				'auth',
			),
			array(
				null,
				'authorize',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				null,
				'auth',
			),
			array(
				null,
				'authorize',
				false,
				false,
				null,
				'auth',
			),
			array(
				null,
				'connect',
				'dummy-authority.com',
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				WP_User::class,
				'auth',
			),
			array(
				null,
				'connect',
				'dummy-authority.com',
				false,
				WP_Error::class,
				'auth',
			),
			array(
				null,
				'connect',
				false,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				WP_Error::class,
				'auth',
			),
			array(
				null,
				'connect',
				false,
				false,
				WP_Error::class,
				'auth',
			),
			array(
				null,
				'something',
				false,
				false,
				WP_Error::class,
				'auth',
			),
			array(
				null,
				'',
				false,
				false,
				null,
				'not_auth',
			),
		);
	}

	/**
	 * @test ID4me_Client::build_login_link()
	 * @dataProvider provider_build_login_link
	 *
	 * @param $authority
	 * @param $identifier
	 * @param $redirect_url
	 * @param $use_case
	 * @param $is_link_empty
	 * @param $expected
	 *
	 * @throws InvalidAuthorityIssuerException
	 */
	public function test_build_login_link( $authority, $identifier, $redirect_url, $use_case, $is_link_empty, $expected ) {
		$claim_list = ID4me_Registration::get_claimlist_for_registration();

		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();
		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );
		$model = new AuthorizationStateModel( $identifier, $use_case );

		if ( 'register' === $use_case ) {
			if ( ! $is_link_empty ) {
				$service->method( 'getAuthorizationUrl' )->willReturn( 'url_claims' );
			} else {
				$service->method( 'getAuthorizationUrl' )->willReturn( '' );
			}
		} else {
			if ( ! $is_link_empty ) {
				$service->method( 'getAuthorizationUrl' )->willReturn( 'url' );
			} else {
				$service->method( 'getAuthorizationUrl' )->willReturn( '' );
			}
		}
		$mocking_functions = array( 'get_claimlist' );
		$id4me_client = $this->_get_client_mock( $service, $mocking_functions );
		$authority = $id4me_client->retrieve_authority_data( $authority, $redirect_url );

		if ( 'register' === $use_case ) {
			$id4me_client->method( 'get_claimlist' )->willReturn( $claim_list );
			$id4me_client->expects( $this->once() )
				->method( 'get_claimlist' );

			$service->expects( $this->once() )
				->method( 'getAuthorizationUrl' )
				->with(
					$id4me_client->get_config_object( $authority->get_configuration() ),
					$authority->get_client_id(),
					$identifier,
					$redirect_url,
					$id4me_client->get_state_from_model( $model ),
					null,
					$claim_list,
				);
		} else {
			$service->expects( $this->once() )
				->method( 'getAuthorizationUrl' )
				->with(
					$id4me_client->get_config_object( $authority->get_configuration() ),
					$authority->get_client_id(),
					$identifier,
					$redirect_url,
					$id4me_client->get_state_from_model( $model ),
					null,
					null,
				);
		}

		$link = $id4me_client->build_login_link( $authority, $identifier, $redirect_url, $use_case );

		$this->assertEquals( $expected, $link );
		if ( null === $expected ) {
			$this->assertGreaterThan( 0, $id4me_client->error_count() );
		}
	}

	/**
	 * Data Provider for test_build_login_link()
	 *
	 * @return array
	 */
	public function provider_build_login_link() {
		return array(
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'register',
				false,
				'url_claims',
			),
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'register',
				true,
				null,
			),
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'connect',
				false,
				'url',
			),
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'connect',
				true,
				null,
			),
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'authorize',
				false,
				'url',
			),
			array(
				'dummy-authority.com',
				'dummy',
				'redirect_url',
				'authorize',
				true,
				null,
			),
		);
	}

	/**
	 * @test ID4me_Client::connect()
	 * @dataProvider provider_connect
	 *
	 * @param $user
	 * @param $identifier_is_empty
	 * @param $connection
	 * @param $expected
	 * @param $expected_is_object
	 */
	public function test_connect( $user, $identifier_is_empty, $connection, $expected, $expected_is_object ) {
		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );
		$service->method( 'discover' )->willReturn( 'dummy-authority.com' );
		$mocked_functions = array( 'register_rp_and_get_authorization_link', 'redirect' );

		$id4me_client = $this->_get_client_mock( $service, $mocked_functions );

		if ( ! $identifier_is_empty ) {
			$id4me_client->method( 'register_rp_and_get_authorization_link' )->willReturn( 'url' );
		} else {
			$id4me_client->method( 'register_rp_and_get_authorization_link' )->willReturn( null );
		}

		$_GET['id4me_action'] = $connection;
		$_GET['id4me_identifier'] = 'id4me_identifier';

		if ( ! $expected_is_object && ! empty( $connection ) ) {
			$id4me_client->expects( $this->once() )
				->method( 'redirect' )
				->with( 'url' );
		}

		$returned_user = $id4me_client->connect( $user );
		if ( $expected_is_object ) {
			$this->assertInstanceOf( $expected, $returned_user );
		} else {
			$this->assertNull( $returned_user );
		}
	}

	/**
	 * @test ID4me_Client::run_auth_flow()
	 * @dataProvider provider_run_auth_flow
	 *
	 * @param $identifier
	 * @param $expected
	 */
	public function test_run_auth_flow( $identifier ) {
		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );
		$service->method( 'discover' )->willReturn( 'dummy-authority.com' );
		$mocked_functions = array(
			'register_rp_and_get_authorization_link',
			'wrap_json_error',
			'wrap_json_success',
		);

		$id4me_client = $this->_get_client_mock( $service, $mocked_functions );
		$_POST[ $identifier ] = $identifier;
		$id4me_client->method( 'register_rp_and_get_authorization_link' )->willReturn( 'url' );

		if ( empty( $identifier ) || ! isset( $_POST['identifier'] ) ) {
			$id4me_client->expects( $this->once() )->method( 'wrap_json_error' );
		} else {
			$id4me_client->expects( $this->once() )->method( 'wrap_json_success' );
		}
		$this->assertNull( $id4me_client->run_auth_flow() );
	}

	/**
	 * Data Provider for test_run_auth_flow()
	 * @return array
	 */
	public function provider_run_auth_flow() {
		return array(
			array(
				'identifier',
			),
			array(
				'',
			),
			array(
				'something_else',
			),
		);
	}

	/**
	 * @test ID4me_Client::register_id4me()
	 * @dataProvider provider_register_id4me
	 *
	 * @param $identifier
	 */
	public function test_register_id4me( $identifier ) {

		$service = $this->_get_service_mock();
		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );

		$mocked_functions = array( 'register_rp_and_get_authorization_link', 'wrap_json_error', 'wrap_json_success' );

		$id4me_client = $this->_get_client_mock( $service, $mocked_functions );
		$id4me_client->method( 'register_rp_and_get_authorization_link' )->willReturn( 'url' );
		$_POST[ $identifier ] = $identifier;

		if ( empty( $identifier ) || ! isset( $_POST['id4me_identifier'] ) ) {
			$service->method( 'getUserInfo' )->willThrowException( new Exception( 'exception' ) );
			$id4me_client->expects( $this->once() )->method( 'wrap_json_error' );
		} else {
			$id4me_client->expects( $this->once() )
				->method( 'wrap_json_success' );
		}
		$_POST[ $identifier ] = $identifier;

		$this->assertNull( $id4me_client->register_id4me() );
	}

	/**
	 * Data Provider for test_register_id4me()
	 *
	 * @return array
	 */
	public function provider_register_id4me() {
		return array(
			array(
				'id4me_identifier',
			),
			array(
				'',
			),
			array(
				'something_else',
			),
		);
	}

	/**
	 * Data Provider for test_connect()
	 *
	 * @return array
	 */
	public function provider_connect() {
		return array(
			array(
				new WP_User(),
				true,
				'connect',
				WP_User::class,
				true,
			),
			array(
				new WP_User(),
				true,
				'',
				WP_User::class,
				true,
			),
			array(
				new WP_User(),
				false,
				'connect',
				WP_User::class,
				true,
			),
			array(
				new WP_User(),
				false,
				'',
				WP_User::class,
				true,
			),
			array(
				null,
				true,
				'connect',
				WP_Error::class,
				true,
			),
			array(
				null,
				true,
				'',
				null,
				false,
			),
			array(
				null,
				false,
				'connect',
				'url',
				false,
			),
			array(
				null,
				false,
				'',
				null,
				false,
			),
		);
	}

	/**
	 * @test ID4me_Client::finish_connect()
	 * @dataProvider provider_finish_connect
	 *
	 * @param $access_data
	 */
	public function test_finish_connect( $access_data ) {
		$service = $this->_get_service_mock();

		$id4me_client = new ID4me_Client( $service );
		$user = $id4me_client->finish_connect( $access_data );

		if ( is_array( $access_data ) && array_key_exists( 'identifier', $access_data ) ) {
			$this->assertInstanceOf( WP_User::class, $user );
		} else {
			$this->assertEquals( null, $user );
		}
	}

	/**
	 * Data Provider for test_finish_connect()
	 * @return array
	 */
	public function provider_finish_connect() {
		return array(
			array(
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
			),
			array(
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
			),
			array(
				'test',
			),
		);
	}

	/**
	 * @test ID4me_Client::finish_registration()
	 * @dataProvider provider_finish_registration
	 *
	 * @param $access_data
	 * @param $register_throws_exception
	 */
	public function test_finish_registration( $access_data, $register_throws_exception ) {
		ID4me_WP_Authority::create_table();
		$service = $this->_get_service_mock();
		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );

		if ( $register_throws_exception ) {
			$service->expects( $this->once() )
				->method( 'getUserInfo' )
				->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->expects( $this->once() )
				->method( 'getUserInfo' )
				->willReturn( $this->_get_user_info() );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority = $this->_get_dummy_wp_authority();
		$data = $id4me_client->finish_registration( $authority, $access_data );
		if ( ! $register_throws_exception ) {
			$this->assertEquals( true, is_array( $data ) );
		} else {
			$this->assertEquals( false, is_array( $data ) );
		}
	}

	/**
	 * Data Provider for test_finish_registration()
	 *
	 * @return array
	 */
	public function provider_finish_registration() {

		return array(
			array(
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				true,
			),
			array(
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
					'identifier'    => 'dummy',
					'iss'           => 'dummy',
					'sub'           => 'dummy',
				),
				false,
			),
		);
	}

	/**
	 * Data Provider for test_discover_authority()
	 *
	 * @return array
	 */
	public function provider_discover_authority() {

		return array(
			array(
				'dummy-authority.com',
				false,
				'dummy-authority.com',
			),
			array(
				null,
				false,
				false,
			),
			array(
				'',
				false,
				false,
			),
			array(
				false,
				false,
				false,
			),
			array(
				'dummy-authority.com',
				true,
				false,
			),
			array(
				'',
				true,
				false,
			),
		);
	}

	/**
	 * @test ID4me_Client::retrieve_authority_data()
	 * @dataProvider provider_retrieve_authority_data
	 *
	 * @param boolean $register_throws_exception
	 *
	 * @throws Exception
	 */
	public function test_retrieve_authority_data( $register_throws_exception ) {
		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();

		$service->method( 'getOpenIdConfig' )
			->willReturn( $this->_get_dummy_configuration() );

		// First call: should register
		if ( $register_throws_exception ) {
			$service->expects( $this->once() )
				->method( 'register' )
				->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->expects( $this->once() )
				->method( 'register' )
				->willReturn( $this->_get_dummy_client() );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority = $id4me_client->retrieve_authority_data( rand() . 'dummy-authority.com', 'dummy-redirect.com' );

		if ( $register_throws_exception ) {
			$this->assertFalse( $authority );

		} else {
			$this->assertInstanceOf( 'ID4me_WP_Authority', $authority );
			unset( $authority );

			// Second call: should load directly without registering
			$authority = $id4me_client->retrieve_authority_data( 'dummy-authority.com', 'dummy-redirect.com' );

			$service->expects( $this->never() )
				->method( 'register' );

			$this->assertInstanceOf( 'ID4me_WP_Authority', $authority );
		}

		ID4me_WP_Authority::delete_table();
	}

	/**
	 * Data Provider for test_retrieve_authority_data()
	 *
	 * @return array
	 */
	public function provider_retrieve_authority_data() {

		return array(
			array(
				false,
			),
			array(
				true,
			),
		);
	}

	/**
	 * @test ID4me_Client::authorize_user()
	 * @dataProvider provider_authorize_user
	 *
	 * @param array|null $access_data
	 * @param Exception|null $exception
	 * @param array|null $authorized_access_data
	 */
	public function test_authorize_user( $access_data, $exception, $authorized_access_data ) {
		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )
			->willReturn( $this->_get_dummy_configuration() );

		if ( null === $exception ) {
			$service->method( 'authorize' )
				->willReturn( $access_data );
		} else {
			$service->method( 'authorize' )
				->willThrowException( $exception );
		}

		$existing_authority = $this->_get_dummy_wp_authority();
		$existing_authority->save();

		$id4me_client = new ID4me_Client( $service );

		$this->assertSame(
			$authorized_access_data,
			$id4me_client->authorize_user(
				$existing_authority,
				'dummy-code'
			)
		);

		ID4me_WP_Authority::delete_table();
	}

	/**
	 * Data Provider for test_authorize_user()
	 *
	 * @return array
	 */
	public function provider_authorize_user() {

		return array(
			array(
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
				),
				null,
				array(
					'id_token'      => '1234',
					'access_token'  => '1234',
					'refresh_token' => '1234',
				),
			),
			array(
				array(),
				null,
				array(),
			),
			array(
				array(),
				new Exception(),
				false,
			),
		);
	}

	/**
	 * @test ID4me_Client::get_param()
	 * @dataProvider provider_get_param
	 *
	 * @param string $param
	 * @param string $value
	 * @param string $final_value
	 */
	public function test_get_param( $param, $value, $final_value ) {

		$_GET[ $param ] = $value;

		$id4me_client = new ID4me_Client(
			new ID4me_WP_Authority()
		);

		$this->assertSame(
			$id4me_client->get_param( $param ),
			$final_value
		);
	}

	/**
	 * Data Provider for test_get_param()
	 *
	 * @return array
	 */
	public function provider_get_param() {

		return array(
			array(
				null,
				null,
				false,
			),
			array(
				'dummy-param',
				'test',
				false,
			),
			array(
				'id4me_identifier',
				'test',
				'test',
			),
			array(
				'id4me_identifier',
				'test',
				'test',
			),
			array(
				'id4me_identifier',
				'https://wikipedia.org',
				'https://wikipedia.org',
			),
			array(
				'id4me_identifier',
				'wikipedia.org',
				'wikipedia.org',
			),
			array(
				'dummy_param',
				'wikipedia.org',
				false,
			),
			array(
				'code',
				'<a href="">test</a>',
				'test',
			),
			array(
				'code',
				"\ntest",
				'test',
			),
		);
	}

	/**
	 * @test ID4me_Client::retrieve_identifier_from_state()
	 * @dataProvider provider_retrieve_identifier_from_state
	 *
	 * @param string $state
	 * @param string $identifier
	 * @param string $origin
	 *
	 * @throws ID4me_Simple_Jwt_Encoding_Exception
	 */
	public function test_retrieve_identifier_from_state( $state, $identifier, $origin ) {
		$service = $this->_get_service_mock();
		$service->method( 'createOpenIdConfigFromJson' )
			->willReturn( $this->_get_dummy_configuration() );
		$id4me_client = new ID4me_Client( $service );

		$model = new AuthorizationStateModel( $identifier, $origin );

		//arrange
		$state = $id4me_client->get_state_from_model( $model );
		$result = array(
			'identifier' => 'id4me_identifier',
			'origin' => 'id4me_origin',
		);

		$authorization_model = $id4me_client->retrieve_authorization_state_from_state_parameter( $state );

		if ( empty( $identifier ) || empty( $origin ) || empty( $state ) ) {
			$this->assertFalse( $authorization_model );
		} else {
			$this->assertSame( $authorization_model->get_identifier(), $result['identifier'] );
			$this->assertSame( $authorization_model->get_origin(), $result['origin'] );
		}
	}

	/**
	 * Data Provider for test_retrieve_identifier_from_state()
	 *
	 * @return array
	 */
	public function provider_retrieve_identifier_from_state() {
		return array(
			array(
				'',
				'',
				'',
			),
			array(
				'testState',
				'id4me_identifier',
				'',
			),
			array(
				'testState',
				'',
				'id4me_origin',
			),
			array(
				'testState',
				'id4me_identifier',
				'id4me_origin',
			),
		);
	}

	/**
	 * @test ID4me_Client::delegate_process()
	 * @dataProvider provider_delegate_process
	 *
	 * @param $identifier
	 * @param bool $throw_exception
	 * @param string $origin
	 * @param $register_throws_exception
	 *
	 * @throws ID4me_Simple_Jwt_Encoding_Exception
	 */
	public function test_delegate_process( $identifier, $throw_exception, $origin, $register_throws_exception ) {
		$redirection = null;

		ID4me_WP_Authority::create_table();

		$service = $this->_get_service_mock();

		$service->method( 'getOpenIdConfig' )
			->willReturn( $this->_get_dummy_configuration() );

		// First call: should register
		if ( $register_throws_exception ) {
			$service->expects( $this->once() )
				->method( 'register' )
				->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->expects( $this->once() )
				->method( 'register' )
				->willReturn( $this->_get_dummy_client() );
		}

		$id4me_client = new ID4me_Client( $service );
		$authority = $id4me_client->retrieve_authority_data( 'dummy-authority.com', 'dummy-redirect.com' );
		$login_link = $id4me_client->build_login_link( $authority, $identifier, 'dummy-redirect.com', $origin );

		if ( ! empty( $login_link ) ) {
			$this->assertTrue( is_string( $login_link ) );

			if ( ! empty( $origin ) ) {
				$this->assertTrue( is_string( $origin ) );
			}
			try {
				$id4me_client->delegate_process(
					$login_link,
					$origin
				);
			} catch ( ErrorException $exception ) {
				if ( $throw_exception ) {
					$this->assertTrue( $throw_exception );
				}
			}
		}
	}

	/**
	 * Data Provider for test_delegate_process()
	 *
	 * @return array
	 */
	public function provider_delegate_process() {

		return array(
			array(
				'test-test.de',
				'http://dummy-url.com',
				false,
				'test',
				false,
			),
			array(
				null,
				true,
				'test',
				true,
			),
		);
	}

	/**
	 * @test ID4me_Client::register_rp_and_get_authorization_link()
	 * @dataProvider provider_register_rp_and_get_authorization_link
	 *
	 * @param $identifier
	 * @param $use_case
	 * @param $throws_exception
	 * @param $returns_empty_link
	 * @param $expected
	 */
	public function test_register_rp_and_get_authorization_link( $identifier, $use_case, $throws_exception, $returns_empty_link, $expected ) {
		$service = $this->_get_service_mock();

		$service->method( 'createOpenIdConfigFromJson' )->willReturn( $this->_get_dummy_configuration() );
		$service->method( 'register' )->willReturn( $this->_get_dummy_client() );

		if ( $throws_exception ) {
			$service->method( 'getOpenIdConfig' )->willThrowException( new Exception( 'exception' ) );
		} else {
			$service->method( 'getOpenIdConfig' )->willReturn( $this->_get_dummy_configuration() );
		}
		if ( $returns_empty_link || $throws_exception ) {
			$service->method( 'getAuthorizationUrl' )->willReturn( '' );
		} else {
			$service->method( 'getAuthorizationUrl' )->willReturn( 'url' );
		}
		$functions_to_mock = array( 'discover_authority' );
		$id4me_client = $this->_get_client_mock( $service, $functions_to_mock );

		if ( empty( $identifier ) ) {
			$id4me_client->method( 'discover_authority' )->willReturn( false );
		} else {
			$id4me_client->method( 'discover_authority' )->willReturn( 'dummy-authority.com' );
		}

		$result = $id4me_client->register_rp_and_get_authorization_link( $identifier, $use_case );

		$this->assertEquals( $expected, $result );
	}

	/**
	 * Data Provider for test_register_rp_and_get_authorization_link()
	 *
	 * @return array
	 */
	public function provider_register_rp_and_get_authorization_link() {

		return array(
			array(
				'test-test.de',
				'register',
				true,
				true,
				null,
			),
			array(
				'test-test.de',
				'register',
				true,
				false,
				null,
			),
			array(
				'test-test.de',
				'register',
				false,
				true,
				null,
			),
			array(
				'test-test.de',
				'register',
				false,
				false,
				'url',
			),
			array(
				'',
				'register',
				true,
				true,
				null,
			),
			array(
				null,
				'register',
				true,
				false,
				null,
			),
			array(
				'',
				'register',
				false,
				true,
				null,
			),
			array(
				'',
				'register',
				false,
				false,
				null,
			),
			array(
				'test-test.de',
				'connect',
				true,
				true,
				null,
			),
			array(
				'test-test.de',
				'connect',
				true,
				false,
				null,
			),
			array(
				'test-test.de',
				'connect',
				false,
				true,
				null,
			),
			array(
				'test-test.de',
				'connect',
				false,
				false,
				'url',
			),
			array(
				'',
				'connect',
				true,
				true,
				null,
			),
			array(
				'',
				'connect',
				true,
				false,
				null,
			),
			array(
				'',
				'connect',
				false,
				true,
				null,
			),
			array(
				'',
				'connect',
				false,
				false,
				null,
			),
			array(
				'test-test.de',
				'authorize',
				true,
				true,
				null,
			),
			array(
				'test-test.de',
				'authorize',
				true,
				false,
				null,
			),
			array(
				'test-test.de',
				'authorize',
				false,
				true,
				null,
			),
			array(
				'test-test.de',
				'authorize',
				false,
				false,
				'url',
			),
			array(
				'',
				'authorize',
				true,
				true,
				null,
			),
			array(
				'',
				'authorize',
				true,
				false,
				null,
			),
			array(
				'',
				'authorize',
				false,
				true,
				null,
			),
			array(
				'',
				'authorize',
				false,
				false,
				null,
			),
		);
	}

	/**
	 * Get Mock Object for Id4me\RP\Service class
	 *
	 * @return \PHPUnit\Framework\MockObject\MockObject
	 */
	private function _get_service_mock() {

		return $this->getMockBuilder( 'Id4me\RP\Service' )
			->disableOriginalConstructor()
			->setMethods(
				array(
					'discover',
					'getOpenIdConfig',
					'register',
					'createOpenIdConfigFromJson',
					'getAuthorizationUrl',
					'authorize',
					'getUserInfo',
				)
			)
			->getMock();
	}

	/**
	 * @param $service
	 *
	 * @return \PHPUnit\Framework\MockObject\MockObject
	 */
	private function _get_client_mock( $service, $functions_to_mock ) {

		return $this->getMockBuilder( 'ID4me_Client' )
			->setConstructorArgs( array( $service ) )
			->setMethods( $functions_to_mock )
			->getMock();
	}

	/**
	 * @return \Id4me\RP\Model\UserInfo
	 */
	private function _get_user_info() {
		$array = array(
			'sub'                => 'dummy',
			'aud'                => 'dummy',
			'iss'                => 'dummy',
			'iat'                => '12345',
			'email'              => 'foo@foo.de',
			'family_name'        => 'Foo',
			'nickname'           => 'test',
			'given_name'         => 'Foo',
			'id4me.identifier'   => 'dummy',
			'website'            => 'testwebsite',
			'preferred_username' => 'dummy',
		);

		return new \Id4me\RP\Model\UserInfo( $array );
	}

	/**
	 * Get Mock Object for our own ID4me_WP_Authority class
	 *
	 * @return ID4me_WP_Authority
	 */
	private function _get_dummy_wp_authority() {

		$authority = new ID4me_WP_Authority();
		$authority->set_id( 1 );
		$authority->set_hostname( 'dummy-authority.com' );
		$authority->set_client_id( 'abcd123' );
		$authority->set_client( serialize( new \Id4me\RP\Model\Client( 'dummy' ) ) );
		$authority->set_client_secret( 'qefeq6g47r8ghhet9rht9rh8t6' );
		$authority->set_configuration( '{[]}' );

		return $authority;
	}

	/**
	 * Generate a Client Model object
	 *
	 * @return \Id4me\RP\Model\Client
	 */
	private function _get_dummy_client() {

		$client = new \Id4me\RP\Model\Client( 'dummy-authority.com' );
		$client->setClientId( 'abcd123' );
		$client->setClientSecret( '00000000000000000000000' );
		$client->setClientExpirationTime( 0 );

		return $client;
	}

	/**
	 * Generate an OpenIdConfig Model object
	 *
	 * @return \Id4me\RP\Model\OpenIdConfig
	 */
	private function _get_dummy_configuration() {

		return new \Id4me\RP\Model\OpenIdConfig( array() );
	}
}
