# The following manual testing steps shall be undertaken for a release or MR

1.	Authorization – Login screen
    1.	Valid login & confirm (happy path) – without remember me
    2.	Valid login & confirm (happy path) – with remember me, retry if works 2nd time
    3.	Remember me – test “forgetting” function
    4.	Invalid login, retry
    5.	Valid login & abort at consent, retry
    6.	Valid login but no user in the system, retry
2.	Authorization – Profile screen
    1.	Valid login & confirm (happy path)
    2.	Invalid login, retry
    3.	Valid login & abort at consent, retry
    4.	Valid login & close window, retry
    5.	Valid login but no user in the system
3.	Registration – Login screen
    1.	Valid login & confirm (happy path)
    2.	Invalid login, retry
    3.	Valid login & abort at consent, retry
    4.	Valid login & close pop-up window, retry
    5.	Valid login but user in the system (iss+sub and/or username), retry
4.	Related regular paths (side effects)
    1.	Create user
    2.	Update user profile
    3.	Login (WP flow)
    4.	Register user (WP flow)
5.	Migration
    1.	Install previous released version, provision ID, Upgrade, test login
    2.	Install previous released version, provision ID, Upgrade, test registration with the same Authority as the first ID
